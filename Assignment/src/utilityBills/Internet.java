package utilityBills;

import java.util.Date;

public class Internet extends Bill {

	private String providerName;
	private double dataUsed;
	
	public Internet(int id, Date bdate, String type, double amount,String provider, double usage ) {
		super(id, bdate, type, amount);
		this.setProviderName(provider);
		this.setDataUsed(usage);
		// TODO Auto-generated constructor stub
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public double getDataUsed() {
		return dataUsed;
	}

	public void setDataUsed(double dataUsed) {
		this.dataUsed = dataUsed;
	}

}
