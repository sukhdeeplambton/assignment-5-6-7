package utilityBills;

import java.util.Date;

public class Hydro extends Bill {
	private String agencyName;
	private double unitConsumed;

	public Hydro(int id, Date bdate, String type, double amount,String agencyn,double unitc) {
		super(id, bdate, type, amount);
		// TODO Auto-generated constructor stub
		this.agencyName=agencyn;
		this.unitConsumed=unitc;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public double getUnitConsumed() {
		return unitConsumed;
	}

	public void setUnitConsumed(double unitConsumed) {
		this.unitConsumed = unitConsumed;
	}
	

}
