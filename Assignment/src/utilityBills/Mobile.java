package utilityBills;

import java.util.Date;

public class Mobile extends Bill {
	
	private String manf_name;
	private String plan;
	private int mobile_no;
	private double data_used;
	private double minute_consumed;
	
	
	
	
	
	public Mobile(int id, Date bdate, String type, double amount,String mname,String pname,int mno,double dataused,double minute ) {
		super(id, bdate, type, amount);
		// TODO Auto-generated constructor stub
		this.data_used=dataused;
				this.minute_consumed=minute;
				this.manf_name=mname;
				this.mobile_no=mno;
				this.plan=pname;
	}


	public String getManf_name() {
		return manf_name;
	}


	public void setManf_name(String manf_name) {
		this.manf_name = manf_name;
	}


	public String getPlan() {
		return plan;
	}


	public void setPlan(String plan) {
		this.plan = plan;
	}


	public int getMobile_no() {
		return mobile_no;
	}


	public void setMobile_no(int mobile_no) {
		this.mobile_no = mobile_no;
	}


	public double getData_used() {
		return data_used;
	}


	public void setData_used(int data_used) {
		this.data_used = data_used;
	}


	public double getMinute_consumed() {
		return minute_consumed;
	}


	public void setMinute_consumed(int minute_consumed) {
		this.minute_consumed = minute_consumed;
	}

}
