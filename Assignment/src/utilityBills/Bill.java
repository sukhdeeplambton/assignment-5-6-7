package utilityBills;

	import java.util.Date;

	public class Bill {

		private int billID;
		private Date billDate;
		private String billType;
		private double totalbillAmount;
		
		public Bill(int id,Date bdate,String type,double amount)
		{
			this.billID=id;
			this.billDate=bdate;
			this.billType=type;
			this.totalbillAmount=amount;
			
		}
		

		public int getBillID() {
			return billID;
		}

		public void setBillID(int billID) {
			this.billID = billID;
		}

		public Date getBillDate() {
			return billDate;
		}

		public void setBillDate(Date billDate) {
			this.billDate = billDate;
		}

		public String getBillType() {
			return billType;
		}

		public void setBillType(String billType) {
			this.billType = billType;
		}

		public double getTotalbillAmount() {
			return totalbillAmount;
		}

		public void setTotalbillAmount(double totalbillAmount) {
			this.totalbillAmount = totalbillAmount;
		}
		

		}







