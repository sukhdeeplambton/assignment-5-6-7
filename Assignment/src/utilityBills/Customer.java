package utilityBills;

import java.util.ArrayList;

public class Customer {
	private int CustomerID;
	private String firstName;
	private String lastNmae;
	private String fullName;
	private String emailID;
	private ArrayList<Bill> List1;
	private double Total;
	public Customer (int cid,String f_name,String l_name,String fl_name,String mail )
	{
		this.CustomerID=cid;
		this.firstName=f_name;
		this.lastNmae=l_name;
		this.fullName=fl_name;
		this.emailID= mail;
		this.List1 = new ArrayList<Bill>();
	}
	public Customer(Bill bill)
	{
		this.List1.add(bill);
	}
	public int getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(int customerID) {
		CustomerID = customerID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getLastNmae() {
		return lastNmae;
	}
	public void setLastNmae(String lastNmae) {
		this.lastNmae = lastNmae;
	}
	public double getTotal() {
		return Total;
	}
	public void setTotal(double total) {
		Total = total;
	}
	
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public ArrayList<Bill> getList1() {
		return List1;
	}
	public void setList1(ArrayList<Bill> list1) {
		List1 = list1;
	}
	public void calculateTotal()
	{
		double total = 0;
		for (int x = 0; x < List1.size(); x++) 
		{

			total += this.List1.get(x).getTotalbillAmount();

		}

		this.setTotal(total);
	}
	public void sorting() 
	{

		for (int x = 1; x < this.List1.size(); x++) 
		{

			for (int y = x; y > 0; y--) 
			{
				if (this.List1.get(y).getBillID() < this.List1.get(y - 1).getBillID()) 
				{
					Bill temp = this.List1.get(y);
					this.List1.set(y, this.List1.get(y - 1));
					this.List1.set(y - 1, temp);

				}
			}
		}

	}

}
